#!/usr/bin/env python3

import sys, os, re
from pathlib import Path

class Dependency:
	def __init__(self, name, aport):
		self.name = name
		self.depended_on_by = [aport]
	
	def __eq__(self, other):
		if not hasattr(other, "name"):
			return False
		
		return self.name == other.name
	
	def __hash__(self):
		return hash(self.name)
	
	def __str__(self):
		string = "testing/" + self.name + " required by"
		for aport in self.depended_on_by:
			string = string + " " + aport
		return string

if len(sys.argv) <= 2:
	print("Please specify the pmports and aports trees (in that order) to check!", file=sys.stderr)
	sys.exit(1)

pmaports_dir = Path(sys.argv[1])
aports_dir = Path(sys.argv[2])

if pmaports_dir.is_file() or aports_dir.is_file():
	print("Please specify directories rather than files!", file=sys.stderr)
	sys.exit(1)

# Get a list of all the aports we have
aports = []
for file in pmaports_dir.glob('**/*'):
	if file.is_file() and file.name == "APKBUILD":
		aports.append(file)

# Get a list of all the aports we depend on
dependencies = []
for aport in aports:
	with open(aport) as f:
		lines = f.readlines()
		
		for line in lines:
			for match in re.search("^depends=\"(.*)\"", line), re.search("^depends_dev=\"(.*)\"", line), re.search("^makedepends=\"(.*)\"", line):
				if match:
					for dependency in match.group(1).split():
						# Remove any suffix it might have
						dependency = dependency.split("-dev")[0]
						dependency = dependency.split("-openrc")[0]
						dependency = dependency.split("-static")[0]
						
						already_found = None
						for package in dependencies:
							if dependency == package.name:
								already_found = package
								break
						
						if already_found != None:
							package.depended_on_by.append(aport.parent.name)
						else:
							dependencies.append(Dependency(dependency, aport.parent.name))
					
					break

# Remove any dependencies which we already have in our own repository
for aport in aports:
	aport = Dependency(aport.parent.name, [])
	if aport in dependencies:
		dependencies.remove(aport)


# Get a list of all the aports in Alpine Linux
del aports[:]
in_testing = []
for file in aports_dir.glob('**/*'):
	if file.is_file() and file.name == "APKBUILD":
		if file.parent.parent.name == "testing":
			aport = Dependency(file.parent.name, [])
			if aport in dependencies:
				in_testing.append(aport)

for dependency in dependencies:
	for aport in in_testing:
		if aport == dependency:
			print(dependency)
